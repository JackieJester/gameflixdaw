﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Persistence
{
    public interface IGameRepository
    {
        #region Games functions
        Game GetGame(int id);
        Game CreateGame(Game game);
        Game UpdateGame(Game game);
        Game RemoveGame(int id);
        IEnumerable<Game> GetGames(bool demo);
        #endregion
        #region Account functions
        Account GetAccount(int id);
        Account CreateAccount(Account account);
        Account UpdateAccount(Account account);
        Account RemoveAccount(int id);
        IEnumerable<Game> GetAccountGames(int accountID);
        #endregion
        #region Reviews area
        Review GetReview(int gameid,int id);
        Review UpdateReview(Review review);
        Review CreateReview(Review review);
        Review RemoveReview(int id);
        IEnumerable<Review> GetReviews(int gameid);
        #endregion

    }
}
