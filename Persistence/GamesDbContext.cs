﻿using API.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Persistence
{
    public class GamesDbContext : DbContext
    {
        public GamesDbContext(DbContextOptions<GamesDbContext> options):base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>().HasData(
                new Game
                {
                    ID = -1,
                    Name = "Warcraft 3",
                    Publisher = "Blizzard",
                    Price = 20,
                    Demo = false,
                    PhotoName = "Warcraft3.jfif"
                },
                new Game
                {
                    ID = -2,
                    Name = "Red Alert 3",
                    Publisher = "EA",
                    Price = 30,
                    Demo = true,
                    PhotoName = "RedAlert.png"
                }

                ) ;
        }
        public DbSet<Account> accounts { get; set; }
        public DbSet<Game> games { get; set; }
        public DbSet<Review> reviews { get; set; }
        
        public DbSet<Subscription> subscription { get; set; }
    }
    
}
