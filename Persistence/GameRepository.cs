﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Persistence
{
    public class GameRepository : IGameRepository
    {
        private GamesDbContext context;

        public GameRepository(GamesDbContext context)
        {
            this.context = context;
        }

        public Account CreateAccount(Account account)
        {
            var entity = context.Add(account);
            context.SaveChanges();
            return entity.Entity;

        }

        public Game CreateGame(Game game)
        {
            var entity = context.Add(game);
            context.SaveChanges();
            return entity.Entity;
        }

        public Review CreateReview(Review review)
        {
            int gameID = review.Game.ID;
            Game g = GetGame(gameID);
            g.Reviews = null;
            review.Game = g;

            var entity = context.Add(review);
            context.SaveChanges();

            Review r = entity.Entity;
            r.Game = null;

            return r;
        }

        public Account GetAccount(int id)
        {
            return context.accounts.FirstOrDefault(a => a.ID == id);   
        }

        public IEnumerable<Game> GetAccountGames(int accountID)
        {
            return GetAccount(accountID).Games;
        }

       /* public IEnumerable<Account> GetAccountsForGame(int gameID)
        {
            return GetGame(gameID).Accounts;
        }
        */
        public Game GetGame(int id)
        {
            return context.games.FirstOrDefault(a => a.ID == id);
        }

        public IEnumerable<Game> GetGames(bool demo)
        {
            return context.games.Where(g=>g.Demo==demo).Select(g=>g).ToList();
        }

        public Review GetReview(int gameid, int id)
        {
            if(gameid == 0)
            {
                return context.reviews.FirstOrDefault(a => a.ID == id);
            }
            if (GetGame(gameid) != null)
            {
                return context.reviews.FirstOrDefault(a => a.ID == id);
            }
                return null;
            
        }

        public IEnumerable<Review> GetReviews(int gameid)
        {
            return context.reviews.Where(r =>r.Game.ID == gameid).Select(r => r).ToList();
        }

        public Account RemoveAccount(int id)
        {
            var account = GetAccount(id);
            context.Remove(account);
            return account;
        }

        public Game RemoveGame(int id)
        {
            var game = GetGame(id);
            context.Remove(game);
            context.SaveChanges();
            return game;
        }

        public Review RemoveReview(int id)
        {
            var review = GetReview(0, id);
            context.Remove(review);
            context.SaveChanges();
            return review;
        }

        public Account UpdateAccount(Account account)
        {
            context.Entry(account).State = EntityState.Modified;
            context.SaveChanges();
            return account;
        }

        public Game UpdateGame(Game game)
        {
            context.Entry(game).State = EntityState.Modified;
            context.SaveChanges();
            return game;
        }

        public Review UpdateReview(Review review)
        {
            context.Entry(review).State = EntityState.Modified;
            context.SaveChanges();
            return review;
        }
    }
}
