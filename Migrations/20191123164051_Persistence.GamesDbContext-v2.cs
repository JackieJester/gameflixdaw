﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class PersistenceGamesDbContextv2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_accounts_games_GameID",
                table: "accounts");

            migrationBuilder.DropIndex(
                name: "IX_accounts_GameID",
                table: "accounts");

            migrationBuilder.DropColumn(
                name: "GameID",
                table: "accounts");

            migrationBuilder.AddColumn<int>(
                name: "AccountID",
                table: "games",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Review",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Contend = table.Column<string>(nullable: true),
                    AccountID = table.Column<int>(nullable: false),
                    GameID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Review", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Review_games_GameID",
                        column: x => x.GameID,
                        principalTable: "games",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_games_AccountID",
                table: "games",
                column: "AccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Review_GameID",
                table: "Review",
                column: "GameID");

            migrationBuilder.AddForeignKey(
                name: "FK_games_accounts_AccountID",
                table: "games",
                column: "AccountID",
                principalTable: "accounts",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_games_accounts_AccountID",
                table: "games");

            migrationBuilder.DropTable(
                name: "Review");

            migrationBuilder.DropIndex(
                name: "IX_games_AccountID",
                table: "games");

            migrationBuilder.DropColumn(
                name: "AccountID",
                table: "games");

            migrationBuilder.AddColumn<int>(
                name: "GameID",
                table: "accounts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_accounts_GameID",
                table: "accounts",
                column: "GameID");

            migrationBuilder.AddForeignKey(
                name: "FK_accounts_games_GameID",
                table: "accounts",
                column: "GameID",
                principalTable: "games",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
