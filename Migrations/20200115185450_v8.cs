﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class v8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "games",
                columns: new[] { "ID", "AccountID", "Demo", "Name", "PhotoName", "Price", "Publisher" },
                values: new object[] { -1, null, false, "Warcraft 3", "Warcraft3.jfif", 20, "Blizzard" });

            migrationBuilder.InsertData(
                table: "games",
                columns: new[] { "ID", "AccountID", "Demo", "Name", "PhotoName", "Price", "Publisher" },
                values: new object[] { -2, null, true, "Red Alert 3", "RedAlert.png", 30, "EA" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "games",
                keyColumn: "ID",
                keyValue: -2);

            migrationBuilder.DeleteData(
                table: "games",
                keyColumn: "ID",
                keyValue: -1);
        }
    }
}
