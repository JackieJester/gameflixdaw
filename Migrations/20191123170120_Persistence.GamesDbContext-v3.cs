﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class PersistenceGamesDbContextv3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Review_games_GameID",
                table: "Review");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Review",
                table: "Review");

            migrationBuilder.RenameTable(
                name: "Review",
                newName: "reviews");

            migrationBuilder.RenameIndex(
                name: "IX_Review_GameID",
                table: "reviews",
                newName: "IX_reviews_GameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_reviews",
                table: "reviews",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_reviews_games_GameID",
                table: "reviews",
                column: "GameID",
                principalTable: "games",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_reviews_games_GameID",
                table: "reviews");

            migrationBuilder.DropPrimaryKey(
                name: "PK_reviews",
                table: "reviews");

            migrationBuilder.RenameTable(
                name: "reviews",
                newName: "Review");

            migrationBuilder.RenameIndex(
                name: "IX_reviews_GameID",
                table: "Review",
                newName: "IX_Review_GameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Review",
                table: "Review",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Review_games_GameID",
                table: "Review",
                column: "GameID",
                principalTable: "games",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
