﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GamesController : ControllerBase
    {
        private readonly IGameRepository repository;
        private readonly ILogger<GamesController> _logger;

        public GamesController(IGameRepository repository, ILogger<GamesController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }

        [HttpGet("type/{demo}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Game>> GetGames(string demo)
        {
            return repository.GetGames(Boolean.Parse(demo)).ToList();
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Game> GetGame(int id)
        {
            Game g = repository.GetGame(id);
            if (g != null)
                return g;

            return NotFound();
        }
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Game> DeleteGame(int id)
        {
            return repository.RemoveGame(id);
        }
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Game> UpdateGame(int id , [FromBody]Game game)
        {
            game.ID = id;
            return repository.UpdateGame(game);
        }
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Game> CreateGame([FromBody]Game game)
        {
            
            return repository.CreateGame(game);
        }
    }
}