﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountsController : ControllerBase
    {
        private readonly IGameRepository repository;
        private readonly ILogger<AccountsController> _logger;

        public AccountsController(IGameRepository repository, ILogger<AccountsController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }
        [HttpGet("{accountID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Account> GetAccount(int accountID) 
        {
            Account account = repository.GetAccount(accountID);
            return account;
        }


    }
}
