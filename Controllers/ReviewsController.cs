﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Persistence;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewsController : ControllerBase
    {
        private readonly IGameRepository repository;
        private readonly ILogger<ReviewsController> _logger;

        public ReviewsController(IGameRepository repository, ILogger<ReviewsController> logger)
        {
            this.repository = repository;
            _logger = logger;
        }

        [HttpGet("all/{gameID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public ActionResult<IEnumerable<Review>> GetReviews(int gameID)
        {
            return repository.GetReviews(gameID).ToList();
        }
        [HttpGet("{ID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Review> GetReview(int ID)
        {

            Review r = repository.GetReview(0, ID);
            if (r == null)
                return NotFound();
            return r;
        }
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Review> CreateReview([FromBody]Review review)
        {
            return repository.CreateReview(review);
        }

        [HttpDelete("{ID}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Review> RemoveReview(int ID)
        {
            return repository.RemoveReview(ID);
        }
        [HttpPut("{ID}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Review> UpdateReview(int id, [FromBody]Review review)
        {
            review.ID = id;
            return repository.UpdateReview(review);
        }

    }
}