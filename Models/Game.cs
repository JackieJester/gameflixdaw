﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Game
    {
        public Game() { }
        public Game(int id, string name, string publisher, bool demo, int price)
        {
            ID = id;
            Name = name;
            Publisher = publisher;
            Demo = demo;
            Price = price;

        }
        [Required]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Publisher { get; set; }
        public bool Demo { get; set; }
        public int Price { get; set; }
        public string PhotoName { get; set; }//to  do update database
        //public ICollection<Account> Accounts { get; set; }
        public ICollection<Review> Reviews { get; set; }

    }
}
