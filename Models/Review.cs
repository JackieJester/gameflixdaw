﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Review
    {
        [Required]
        public int ID { get; set; }
        public string Contend { get; set;}
        public int AccountID { get; set; }
        public Game Game { get; set; }

    }
}
