﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Account
    {
        public Account() { }
        public Account(int id, string name, string email, string password, string type)
        {
            ID = id;
            Name = name;
            Email = email;
            Password = password;
            Type = type;
        }
        [Required]
        public int ID { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
        public ICollection<Game> Games { get; set; }
    }
}
